const express = require('express');
const AuthController = require('./controller/AuthController');
const NotebookController = require('./controller/NotebookController');
const MailerController = require('./controller/MailerController');
const UserController = require('./controller/UserController');
const authMiddleware = require('./middlewares/auth');
const SummaryController = require('./controller/SummaryController');
const PagesController = require('./controller/PagesController');
const NoteController = require('./controller/NoteController');
const ScheduleController = require('./controller/ScheduleController');
const LinkController = require('./controller/LinkController');
const TrashController = require('./controller/TrashController');


const routes = express.Router();
routes.post("/auth",AuthController.auth);
routes.post('/forgot-password',MailerController.index);

// apartir dessas rotas precisa de token
routes.use(authMiddleware)

routes.get("/user",UserController.index);
routes.post("/user",UserController.create);
// routes.put("/user",UserController.update);
// routes.delete("/user",UserController.delete);

routes.get('/caderno', NotebookController.index);
routes.post('/caderno', NotebookController.create);
routes.put('/caderno', NotebookController.update);
routes.delete('/caderno', NotebookController.delete);
routes.delete('/caderno/share', NotebookController.delete_share);
routes.post('/caderno/share', NotebookController.share);
routes.get('/caderno/share', NotebookController.share_users);

routes.get('/summary', SummaryController.index);
routes.post('/summary', SummaryController.create);
routes.get('/summary/pages', SummaryController.pages);

routes.get('/pages',PagesController.index);
routes.post('/pages',PagesController.create);
routes.post('/single_pages',PagesController.create_page);
routes.put('/pages',PagesController.update);
routes.delete('/pages',PagesController.delete);

routes.get('/notes', NoteController.index);
routes.post('/notes', NoteController.create);
routes.put('/notes', NoteController.update);
routes.delete('/notes', NoteController.delete);
routes.delete('/notes/share', NoteController.delete_share);
routes.post('/notes/share', NoteController.share);
routes.get('/notes/share', NoteController.share_users);

routes.get('/schedule', ScheduleController.index);
routes.post('/schedule', ScheduleController.create);
routes.put('/schedule', ScheduleController.update);
routes.delete('/schedule', ScheduleController.delete);

routes.post('/link/notebook/:id',LinkController.notebook);

routes.get('/trash', TrashController.index);
routes.post('/trash', TrashController.restore);
routes.delete('/trash', TrashController.delete);
module.exports = routes;
