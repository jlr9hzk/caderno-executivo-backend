const express =  require('express');
const cors = require('cors');
const routes = require("./routes");

const app = express();

app.use(cors());

const allowedOrigins = [
  '*',
  'http://localhost:8888',
  'http://45.15.25.46:8888'
];

// Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
const corsOptions = {
  origin: (origin, callback) => {
    if (allowedOrigins.includes(origin) || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Origin not allowed by CORS'));
    }
  }
}

// Enable preflight requests for all routes
app.options('*', cors(corsOptions));

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: true ,limit: '50mb'}))
app.use(routes);

app.listen(process.env.PORT || 8888, () => console.log("Servidor rodando"))