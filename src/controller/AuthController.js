const knex = require('../database');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const authConfig = require('../config/auth.json');

function generateToken(params = {}){
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400
  })
}

module.exports = {
  async auth(req, res){
    const { email, password } = req.body;

    try {
      const user =  await knex.select('*').from('users').where({email});
      

      if(!user[0])
        return res.status(400).json({error: 'User not found'});
      
      if(!await bcrypt.compare(password, user[0].password)){

        return res.status(400).json({error: 'Password invalid'});
      }

      let initial = user[0].nome.substring(0,1);
      user[0].password = undefined;
      user[0].initial = initial;

      return res.json({
        user,
        token: generateToken({ id: user.id })
      })
    } catch (error) { 
      return res.status(400).json({error: 'Erro ao tentar logar'});
    }
  }
}