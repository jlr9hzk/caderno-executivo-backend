const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const knex = require('../database');

module.exports = {
    async index(req, res){

        const email = req.query.email;

        try {
            //NOVA SENHA
            var senha = Math.floor(Math.random() * 999999);

            const senha_c = await bcrypt.hash(""+senha,10);
            
            if(email != null){
                const response = await knex.select('id','nome','email').table('users').where("email","=",email);
                const id = response[0].id;
                if(response[0].email != ''){
                    var transporter = nodemailer.createTransport({
                        host: 'smtp.hostinger.com.br',
                        port: 587,
                        secure: false,
                        requireTLS: true,
                        auth: {
                          user: 'caderno-executivo@evolutionsoft.com.br',
                          pass: 'Teste123'
                        }
                      });
                      
                    

                    var mailOptions = {
                        from: 'caderno-executivo@evolutionsoft.com.br',
                        to: response[0].email,
                        subject: 'Redefinir Senha - CADERNO EXECUTIVO CREA-SP',
                        text: 'Olá '+response[0].nome+'.\nOuvimos dizer que você esqueceu sua senha. \nAbaixo segue sua nova senha.\n\n\ Senha: '+senha
                      };
                      
                      transporter.sendMail(mailOptions, async function(error, info){
                        if (error) {
                            return res.json({response: error})
                        } else {
                            const resp = await knex('users').update({password: senha_c}).where({id});
                            
                            return res.json({response: 'Enviamos sua nova senha em seu email, por favor verifique sua caixa de entrada!!!'});
                        }
                      });
                }else{
                    return res.send("Usuario não encontrado!!!")
                }
            }
            
        } catch (error) {
            return res.send("Usuario não encontrado!!!")
        }

        
    }
}