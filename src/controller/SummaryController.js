const knex = require('../database');

module.exports = {
  async index(req, res){
    const notebookId = req.query.notebookId;

    try {
      const sections = await knex.select('id','uuid','title').from('sections')
            .where('notebooks_id',notebookId)
            .whereNull('deleted_at');

      for(let i = 0; i < sections.length ; i++){
        const chapters = await knex.select('uuid as id','title').from('chapters').where('sections_id',sections[i].id).whereNull('deleted_at');
        sections[i].id = sections[i].uuid;
        sections[i].uuid = undefined;
        sections[i]['chapters'] = chapters;
      }

      return res.json({sections})
    } catch (error) {
      return res.json(error)
    }
  },
  async create(req, res) {
    const { notebookId, sections } = req.body;

    try {

      const sectionsUuids = sections.map((item) => {
        return item.id
      })

      //deleta sessoes
      const deleted_sections = await knex('sections').update({deleted_at: knex.fn.now()}).whereNotIn('uuid', sectionsUuids).where('notebooks_id', notebookId).whereNull('deleted_at');
      if(deleted_sections) {
        const select_sections = await knex.select('*').from('sections').whereNotIn('uuid', sectionsUuids).where('notebooks_id', notebookId);
        select_sections.forEach( async (item) => {
          await knex('trash').insert({type: 'sections', title: item.title, uuid: item.uuid });
        });
      }


      
      //pega as seçoes do caderno
      sections.forEach(async (section, index_section) => {

        const sectionExist = await knex.select('id').from('sections').where('uuid', section.id);

        if (sectionExist.length == 0) {
          let sectionsId = await knex('sections')
            .insert({ notebooks_id: notebookId, title: section.title, uuid: section.id })
            .returning('id');

          if (!sectionsId)
            return res.json({ response: "Erro na " + index + " seção!!!", status: 400 });

          // pega o json de capitulos de cada seção
          const chapters = sections.map((items) => { return items.chapters; })[index_section];


          chapters.forEach(async (chapter) => {

            let chaptersId = await knex('chapters')
              .insert({ sections_id: sectionsId, title: chapter.title, uuid: chapter.id })
              .returning('id');

            if (!chaptersId)
              return res.json({ response: "Erro ao tentar adicionar capitulo!!!", status: 400 });
          });

        } else {

          const chapterUuids = section.chapters.map((item) => {
            return item.id
          });
          
          const deleted_chapters = await knex('chapters').update({deleted_at: knex.fn.now()}).whereNotIn('uuid', chapterUuids).where('sections_id', sectionExist[0].id).whereNull('deleted_at');
          console.log(deleted_chapters)
          if(deleted_chapters){
            const select_chapters = await knex.select('*').from('chapters').whereNotIn('uuid', chapterUuids).where('sections_id', sectionExist[0].id);
            select_chapters.forEach( async (item) => {
              await knex('trash').insert({type: 'chapters', title: item.title, uuid: item.uuid });
            });
          }

          await knex('sections')
            .update({ notebooks_id: notebookId, title: section.title, uuid: section.id }).where('uuid', section.id)

          const sectionsId = await knex.select('id').from('sections').where('uuid', section.id);

          if (!sectionsId)
            return res.json({ response: "Erro na " + index + " seção!!!", status: 400 });

          // pega o json de capitulos de cada seção
          const chapters = sections.map((items) => {
            return items.chapters;
          })[index_section];


          chapters.forEach(async (chapter) => {
            const chapterExist = await knex.select('id').from('chapters').where('uuid', chapter.id);


            if (chapterExist.length == 0) {
              let chaptersId = await knex('chapters')
                .insert({ sections_id: sectionsId[0].id, title: chapter.title, uuid: chapter.id })
                .returning('id');

              if (!chaptersId)
                return res.json({ response: "Erro ao tentar adicionar capitulo!!!", status: 400 });
            } else {

              let chaptersId = await knex('chapters')
                .update({ sections_id: sectionsId[0].id, title: chapter.title, uuid: chapter.id }).where('uuid', chapter.id)


              if (!chaptersId)
                return res.json({ response: "Erro ao tentar adicionar capitulo!!!", status: 400 });
            }

          });
        }
      });
      return res.json({ response: "Salvo com Sucesso!!!", status: 200 })
    } catch (error) {
      return res.json({ response: error, status: 400 });
    }

  },
  async pages(req, res){
    const notebookId  = req.query.notebookId;

    try {
      const sections = await knex.select('id','uuid','title').from('sections').where('notebooks_id',notebookId).whereNull('deleted_at');

      for(let i = 0; i < sections.length ; i++){
        const chapters = await knex.select('id','uuid','title').from('chapters').where('sections_id',sections[i].id).whereNull('deleted_at');

        sections[i]['id'] = undefined;


        for(let j = 0; j < chapters.length; j++){

          const pages = await knex.select('id','uuid','title').from('pages').where('chapters_id',chapters[j].id).whereNull('deleted_at');

          chapters[j]['pages'] = pages
        }

        sections[i]['chapters'] = chapters;
      }



      return res.json({sections})
    } catch (error) {
      return res.json(error)
    }
  }
}
