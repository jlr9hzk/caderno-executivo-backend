const knex = require('../database');
module.exports = {
  async index(req, res){
    const user_id = req.query.user_id;

    const response = await 
knex.raw(`SELECT
                t.*
                  FROM 
                      trash as t 
                      left join (
                          select 
                              c.uuid,
                              n.owner_id
                          from 
                              chapters as c 
                                  left join sections as s on 
                              c.sections_id = s.id     
                                  left join notebooks as n ON
                              s.notebooks_id = n.id
                      ) chapter on 
                      chapter.uuid = t.uuid
                      
                      left join (
                          select 
                              n.uuid,
                              n.owner_id
                          from 
                              notebooks as n
                      ) notebook on 
                      notebook.uuid = t.uuid
                      
                      left join (
                          select 
                              s.uuid,
                              n.owner_id
                          from 
                              sections as s
                                  left join notebooks as n on
                              s.notebooks_id = n.id
                      ) section on 
                      section.uuid = t.uuid
                      
                      left join (
                          select 
                              p.uuid,
                              n.owner_id
                          from 
                              pages as p 
                                  left join chapters as c ON
                              p.chapters_id = c.id 
                                  left join sections as s ON
                              c.sections_id = s.id
                                  left join notebooks as n ON
                              s.notebooks_id = n.id
                      ) page1 on 
                      page1.uuid = t.uuid
                      
                  WHERE
                      notebook.owner_id = ${user_id} or chapter.owner_id = ${user_id} or section.owner_id = ${user_id} or page1.owner_id = ${user_id} group by t.uuid`);
                    
    return res.json(response[0]);
  },

  async restore(req, res){
    const {uuid, type } = req.body;

    try {
      switch(type){
        case 'notebooks':
          await knex('notebooks').update({deleted_at: null}).where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid)
          break;
        case 'sections':
          await knex('sections').update({deleted_at: null}).where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid)
          break;  
        case 'chapters':
          await knex('chapters').update({deleted_at: null}).where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid)
          break;
        case 'pages':
          await knex('pages').update({deleted_at: null}).where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid)
          break;
        default:
          break;
      }

      
      return res.json({response: 'Restaurado com sucesso!!!'});
    } catch (error) {
      return res.json({response: error})
    }
  },

  async delete(req, res){
    const uuid = req.query.uuid;
    const type = req.query.type;

    try {
      switch(type){
        case 'notebooks':
          await knex('notebooks').delete().where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid);
          break;
        case 'sections':
          await knex('sections').delete().where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid);
          break;  
        case 'chapters':
          await knex('chapters').delete().where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid);
          break;
        case 'pages':
          await knex('pages').delete().where('uuid',uuid);
          await knex('trash').delete().where('uuid',uuid);
          break;
        default:
          break;
      }
      return res.json({response: 'Deletado com sucesso!!!'});
    } catch (error) {
      return res.json({response: error})
    }
  }
}