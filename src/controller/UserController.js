const knex = require('../database');
const bcrypt = require('bcryptjs');


module.exports = {
  async index(req, res) {
    
    try {
      const response = await knex.select('*').from('users').where('ativo',1);
      for(let i = 0; i < response.length; i++){
        response[i].password = undefined;
      }
      return res.json(response);
    } catch (error) {
      return res.json({error: 'Erro ao listar usuario'});
    }
  },

  async create(req, res){
    const {nome, user, password, email} = req.body;

    try {
      
      bcrypt.hash(password, 10, async (err, hash) => {
        const response = await knex('users').insert({nome, user, password: hash, email, ativo: 1});


        return res.json({response: 'Usuário cadastrado com sucesso!!!'});
      });

    } catch (error) {
      return res.json({response: 'Erro ao cadastrar usuário!!!'});
    }
  }
}