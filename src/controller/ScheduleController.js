const knex = require('../database');

module.exports = {
  async index(req, res){
    const user_id = req.query.userId;

    try {
      const schedules = await knex
            .select('schedule.uuid','schedule.title','schedule.start','schedule.end','schedule.all_day',
                    'schedule.type','schedule.hour','schedule.estimated_hours','schedule.repeat','schedule.description','schedule.owner_id')
            .from('schedule')
            .leftJoin('schedule_users','schedule.id','schedule_users.schedule_id')
            .where('schedule_users.users_id',user_id)
            .orWhere('schedule.owner_id',user_id)
            .groupBy('schedule.id');
      return res.json({schedules});
    } catch (error) {
      return res.json(error);
    }
  },
  async create(req, res){
    const { uuid, notebook_id, title, start, end, allDay, type, hour, estimated_hours, repeat, description,ownerId, users } = req.body;

    try {
      const scheduleId = await knex('schedule')
          .insert({uuid, title, start, end, all_day: allDay, type, hour, estimated_hours, repeat, description, notebooks_id: notebook_id,owner_id: ownerId }).returning('id')
      
      for(let i = 0;i < users.length; i++ ){
        await knex('schedule_users').insert({schedule_id: scheduleId, users_id: users[i].user_id })
      }

      return res.json({response: 'Agendamento criado com sucesso!!!', status: 200});
    } catch (error) {
      return res.json({response: error, status: 400});
    }
  },
  async update(req, res){
    const { uuid, notebook_id, title, start, end, allDay, type, hour, estimated_hours, repeat, description, users } = req.body;

    try {
      const schedule = await knex('schedule')
          .update({title, start, end, all_day: allDay, type, hour, estimated_hours, repeat, description, notebooks_id: notebook_id })
          .where('uuid',uuid);
      if(!schedule) return res.json({response: 'Erro ao alterar evento!!!', status: 400});

      const scheduleId = await knex.select('id').from('schedule').where('uuid',uuid);

      await knex('schedule_users').delete().where('schedule_id',scheduleId[0].id);

      for(let i = 0;i < users.length; i++ ){
        await knex('schedule_users').insert({schedule_id: scheduleId[0].id, users_id: users[i].user_id })
      }

      return res.json({response: 'Agendamento alterado com sucesso!!!', status: 200});
    } catch (error) {
      return res.json({response: error, status : 400})
    }
  },
  async delete(req, res){
    const uuid = req.query.uuid;

    try {
      const schedule = await knex('schedule').delete().where('uuid',uuid);
      if(!schedule) return res.json({reposnse: 'Agendamento inexistente!!!', status: 200})
      
      return res.json({response: 'Agendamento deletado com sucesso!!!', status: 200});
    } catch (error) {
      return res.json({response: error, status: 400});
    }
  },
}