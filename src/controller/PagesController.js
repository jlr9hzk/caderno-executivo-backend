const knex = require('../database');
const fs = require("fs");
module.exports = {
  async index(req, res){
    const uuid = req.query.uuid;

    try {
      const response = await knex.select('title','content_html').from('pages').where('uuid',uuid).whereNull('deleted_at');

      return res.json(response);
    } catch (error) {
      return res.json(error);
    }
  },
  async create_page(req, res){
    const { chapter_uuid, uuid, title, content_html } = req.body;

    try {
      const chapter = await knex.select('id').from('chapters').where('uuid',chapter_uuid);

      if(chapter){
        const page = await knex('pages').insert({uuid,title,content_html, chapters_id: chapter[0].id});

        return res.json({response: 'Pagina criada com sucesso!!!'});
      }else{
        return res.json({response: 'Capítulo inexistente!!!'})
      }
    } catch (error) {
      return res.json(error.message);
    }
  },
  async create(req, res) {
    const { uuid, pdf, numberPages } = req.body;

    const path_server = "http://evolutionsoft.dev.br/caderno-backend/pdf/";
    // fs.unlink("../pdf/" + uuid + ".pdf", function(err){

    //   // Ignore error if no file already exists
    //   if (err && err.code !== 'ENOENT')
    //     return res.json({ response: "Erro ao gravar pdf!!!" })

    fs.writeFile("./pdf/" + uuid + ".pdf", pdf, 'base64', function (err) {
      if (err)
        return res.json({ response: "Erro ao gravar pdf!!! "+err })
    });
    // });
    let path_file = path_server + uuid + ".pdf";
    const resp = await knex('notebooks').update({ pdf: path_file, number_pages: numberPages }).where('uuid', uuid);

    if (resp) {

      // let error = [];
      // let text_error = "";
      // content.map(async (items, index) => {
      //   const chapterId = await knex.select('id').from('chapters').where('uuid', items.chapterId);

      //   content[index].pages.forEach(async (element) => {
      //     const existPages = await knex.select('id').from('pages').where('uuid',element.id);
      //     if(existPages.length == 0){
      //       let resp = await knex('pages').insert({ title: element.title, content_html: element.content_html, uuid: element.id, chapters_id: chapterId[0].id });
            
      //       if (!resp) {
      //         error.push("Erro na pagina - " + element.title);
      //       }
      //     }else{
      //       let resp = await knex('pages').update({ title: element.title, content_html: element.content_html }).where('uuid', element.id);
            
      //       if (!resp) {
      //         error.push("Erro na pagina - " + element.title);
      //       }
      //     }
          

      //   });

      // })

      // if (error.length > 0) {
      //   error.forEach((err) => {
      //     text_error += err + "</br>";
      //   })
      //   return res.json({ response: text_error, status: 400 })
      // }
      return res.json({ response: "Páginas sincronizadas com sucesso!!!", status: 200 })

    }else{
      return res.json({response: "Erro ao tentar gravar arquivo!!!", status: 400})
    }
  },
  async update(req, res){
    const {uuid, title, content_html } = req.body;

    try {
      const updatePage = await knex('pages').update({title,content_html}).where('uuid',uuid);

      if(!updatePage) return res.json({response: 'Erro ao atualizar página', status:400 });

      return res.json({response: 'Página atualizada com sucesso!!!', status: 200});
    } catch (error) {
      return res.json({response: error, status: 400});
    }
  },
  async delete(req, res){
    const uuid  = req.query.uuid;

    try {
      const deletePage = await knex('pages').update({deleted_at: knex.fn.now()}).where('uuid',uuid);
      
      if(!deletePage) return res.json({response: 'Erro ao deletar página', status:400 });

      const pages = await knex.select('title').from('pages').where('uuid', uuid);
      await knex('trash').insert({type: 'pages', title: pages[0].title, uuid});

      return res.json({response: 'Página deletada com sucesso!!!', status: 200});
    } catch (error) {
      return res.json({response: error, status: 400});
    }
  }
}
