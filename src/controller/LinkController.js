const knex = require('../database');
const moment = require('moment');

module.exports = {
  async notebook(req, res){
    const { id } = req.params;
    const { user_id } = req.body;
    
    try {
      const notebook = await knex.select('*').from('notebooks_users').where('users_id',user_id).where('notebooks_id',id);

      if(notebook.length == 0 ) return res.json({auth: false, response: null});
      

      const response = await knex
      .select('notebooks.id as caderno_id', 'notebooks.nome as nome_caderno', 'notebooks.owner_id',
        'notebooks.uuid', 'notebooks.create_at',
        'notebooks.update_at as ultima_alteracao',
        'notebooks.foto')
      .from('notebooks')
      .where('notebooks.id',id)

      for (let i = 0; i < response.length; i++) {
        if (response[i]['ultima_alteracao'] == null) {
          response[i]['ultima_alteracao'] = moment(response[i]['create_at']).format('DD/MM/YYYY');
        } else {
          response[i]['ultima_alteracao'] = moment(response[i]['ultima_alteracao']).format('DD/MM/YYYY');
        }

        if (!response[i]['foto']) {
          response[i]['foto'] = "https://i.imgur.com/bgoa1N1.png";
        }else{
          response[i]['foto'] = String.fromCharCode.apply(null, new Uint16Array(response[i]['foto']));
        }
      }
      return res.json({auth: true, response});
      
    } catch (error) {
      return res.json({auth: error});
    }
  }
}