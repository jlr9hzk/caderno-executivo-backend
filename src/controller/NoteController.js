const knex = require('../database');
const nodemailer = require('nodemailer');
const moment = require('moment');
module.exports = {
  async index(req , res){
    const userId = req.query.userId;

    try {
      const notes = await knex
            .select('notes.id as notesId',
                    'notes.uuid',
                    'notes.title',
                    'notes.color',
                    'notes.reminder_date',
                    'notes.reminder_repeat',
                    'notes.create_at as date')
            .from('notes')
            .innerJoin('notes_users','notes.id','notes_users.notes_id')
            .innerJoin('users','notes_users.users_id','users.id')
            .where('users.id',userId)
            .where('users.ativo',1);

      for(let i = 0; i < notes.length; i ++){
          const blocks = await knex
          .select('notes_blocks.text',
                  'notes_blocks.type',
                  'notes_blocks.check',
                  'notes_blocks.uuid')
          .from('notes')
          .innerJoin('notes_users','notes.id','notes_users.notes_id')
          .innerJoin('users','notes_users.users_id','users.id')
          .innerJoin('notes_blocks','notes.id','notes_blocks.notes_id')
          .where('notes.id',notes[i].notesId)
          .where('users.id',userId);

          notes[i].notesId = undefined;
          notes[i].content = blocks;
      }

      return res.send(notes);

    } catch(error){
      return res.json(error)
    }
  },

  async create(req, res){
    const { uuid, title, color, reminder_date , reminder_repeat, notebookId, userId, date, content} =  req.body;
    let reminder_date_c = reminder_date != '' ? moment(reminder_date).format('YYYY-MM-DD HH:mm:ss') : null;

    let reminder_repeat_c = reminder_repeat != '' ? reminder_repeat : null;
    try {
      const noteId = await knex('notes')
                            .insert({uuid,title,color,reminder_date: reminder_date_c,reminder_repeat: reminder_repeat_c,notebooks_id: notebookId});

      if(noteId){
        const noteUser = await knex('notes_users')
                            .insert({notes_id: noteId[0],users_id: userId, permission: 'owner'});
        
        if(!noteUser) return res.json({response: 'Erro ao criar nota, tente novamente!!!', status: 400});
      }

      content.map(async (value)=>{
        await knex('notes_blocks')
              .insert({text: value.text, type: value.type, check: value.check,uuid: value.uuid, notes_id: noteId[0]});
      })

      return res.json({response: 'Nota cadastrada com sucesso!!!', status: 200})
    } catch (error) {
      return res.json({response: error, status: 400});
    }
  },

  async update(req, res){
    const { uuid, title, color, reminder_date, reminder_repeat, content } =  req.body;
    let reminder_date_c = reminder_date != '' ? moment(reminder_date).format('YYYY-MM-DD HH:mm:ss') : null;
    let reminder_repeat_c = reminder_repeat != '' ? reminder_repeat : null;

    try {
      const notes = await knex('notes')
      .update({title,color,reminder_date: reminder_date_c,reminder_repeat}).where({uuid});

      if(notes){
        const notesId = await knex.select('id').from('notes').where('uuid',uuid);
        
        if(notesId){
          await knex('notes_blocks').delete().where('notes_blocks.notes_id',notesId[0].id);
  
          content.map(async (value)=>{
            await knex('notes_blocks')
                  .insert({text: value.text, type: value.type, check: value.check,uuid: value.uuid, notes_id: notesId[0].id});
                  
          })
          return res.json({response: 'Nota alterada com sucesso!!!', status: 200});
          
        }
      }
     


      return res.json({response: 'Erro ao alterar nota', status: 400});

      
    } catch (error) {
      return res.json({response: error, status: 400});
    }
  },
  async delete(req, res){
    const uuid = req.query.uuid;

    try {
      const notes = await knex('notes').delete().where('uuid',uuid);
      if(!notes) return res.json({reposnse: 'Nota inexistente!!!', status: 200})
      
      return res.json({response: 'Nota deletada com sucesso!!!', status: 200});
    } catch (error) {
      return res.json({response: error, status: 400});
    }
  },

  async share(req, res){
    const { users } = req.body;


    users.forEach(async (element) => {
      const { users_id, uuid, destination_user_id, name, email, permission, messege } = element;
      try {
        if (uuid && destination_user_id) {

          const noteId = await knex.select('id').from('notes').where('uuid',uuid);
          const notes_id = noteId[0].id;
          await knex('notes_users').delete()
            .where({ notes_id }).where({ users_id: destination_user_id });

          const resp = await knex('notes_users')
            .insert({ notes_id, users_id: destination_user_id, permission });
          if (resp) {

            const user = await knex.select('nome').from('users').where('id', users_id);
            const notes = await knex.select('title').from('notes').where('id', notes_id);

            var transporter = nodemailer.createTransport({
              host: 'smtp.hostinger.com.br',
              port: 587,
              secure: false,
              requireTLS: true,
              auth: {
                user: 'caderno-executivo@evolutionsoft.com.br',
                pass: 'Teste123'
              }
            });
            var mailOptions = {
              from: 'caderno-executivo@evolutionsoft.com.br',
              to: email,
              subject: 'Compartilhamento - CADERNO EXECUTIVO CREA-SP',
              html: 'Olá <b>' + name + '</b>.</br><b>' + user[0].nome + '</b> compartilhou a nota <b>' + notes[0].title + '</b> com você . </br>Abaixo segue o link de acesso.</br></br> <a style="background: #ffc107; color: black;border-radius: 5px; padding: 5px; text-decoration: none; font-family: sans-serif; font-size: 12px"href="http://caderno.evolutionsoft.com.br">Acessar Nota</a></br></br> ' + messege
            };

            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                return res.json({ response: error })
              } else {
                return res.json({ response: 'Nota compartilhada com sucesso!!!', status: 200 });
              }
            });
          }else{
            return res.json({ response: "Erro ao compartilhar nota!!!", status: 400 });
          }
        } else {
          return res.json({ response: "Erro ao compartilhar nota!!!", status: 400 })
        }
      } catch (error) {
        return res.json({error})
      }
    });
  },
  async delete_share(req, res){
    const user_id = req.query.user_id;
    const uuid = req.query.uuid;

    try {
       const note_id = await knex.select('id').from('notes').where('uuid',uuid);

      const note = await knex('notes_users').delete().where('notes_id',note_id[0].id).where('users_id',user_id);

      if(!note) return res.json({response: 'Erro ao tentar deletar compatilhamento!!!', status: 400});

      return res.json({response: 'Compartilhamento deletado com sucesso!!!', status: 200})
    } catch (error) {
      return res.json({response: error, status: 400})
    }
  },
  async share_users(req, res){
    const uuid = req.query.uuid;

    try {
      const note_id = await knex.select('id').from('notes').where('uuid',uuid);
     
      // id,nome,email,inicial
      const response = await knex.select('users.id','users.nome','users.email','notes_users.permission')
              .from('notes_users')
              .innerJoin('users','notes_users.users_id','users.id')
              .where('notes_users.notes_id',note_id[0].id);

      for(let i = 0; i < response.length ; i++){
        response[i].initial = response[i].nome.substring(0,1);
      }
      return res.json(response)
    } catch (error) {
      return res.json({error : error})
    }
  }
}
