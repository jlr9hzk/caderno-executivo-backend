const moment = require('moment');
const knex = require('../database');
const nodemailer = require('nodemailer');

module.exports = {
  async index(req, res) {
    const user_id = req.query.user_id;
    const notebook_id = req.query.notebook_id;

    try {
      if(notebook_id){
        const response = await knex
        .select('notebooks.id as caderno_id', 'notebooks.nome as nome_caderno', 'notebooks.owner_id',
          'notebooks.descricao','notebooks.number_pages','notebooks.pdf',
          'notebooks.uuid', 'notebooks.create_at',
          'notebooks.update_at as ultima_alteracao', 'notebooks_users.permission',
          'notebooks.foto', 'users.nome', 'users.email')
        .from('notebooks_users')
        .innerJoin('notebooks', 'notebooks.id', 'notebooks_users.notebooks_id')
        .innerJoin('users', 'notebooks_users.users_id', 'users.id')
        .where('notebooks_users.users_id', user_id)
        .whereNull('notebooks.deleted_at')
        .where('notebooks.id',notebook_id)
        .groupBy('notebooks_users.notebooks_id');
  
        for (let i = 0; i < response.length; i++) {
          if (!response[i]['ultima_alteracao']) {
            response[i]['ultima_alteracao'] = moment(response[i]['create_at']).format('DD/MM/YYYY');
          } else {
            response[i]['ultima_alteracao'] = moment(response[i]['ultima_alteracao']).format('DD/MM/YYYY');
          }
  
          if (!response[i]['foto']) {
            response[i]['foto'] = "https://i.imgur.com/bgoa1N1.png";
          }else{
            response[i]['foto'] = String.fromCharCode.apply(null, new Uint16Array(response[i]['foto']));
          }
        }
  
        return res.json(response);
      }else{
        const response = await knex
        .select('notebooks.id as caderno_id', 'notebooks.nome as nome_caderno', 'notebooks.owner_id',
          'notebooks.descricao','notebooks.number_pages','notebooks.pdf',
          'notebooks.uuid', 'notebooks.create_at',
          'notebooks.update_at as ultima_alteracao', 'notebooks_users.permission',
          'notebooks.foto', 'users.nome', 'users.email')
        .from('notebooks_users')
        .innerJoin('notebooks', 'notebooks.id', 'notebooks_users.notebooks_id')
        .innerJoin('users', 'notebooks_users.users_id', 'users.id')
        .where('notebooks_users.users_id', user_id)
        .whereNull('notebooks.deleted_at')
        .groupBy('notebooks_users.notebooks_id');
  
        for (let i = 0; i < response.length; i++) {
          if (!response[i]['ultima_alteracao']) {
            response[i]['ultima_alteracao'] = moment(response[i]['create_at']).format('DD/MM/YYYY');
          } else {
            response[i]['ultima_alteracao'] = moment(response[i]['ultima_alteracao']).format('DD/MM/YYYY');
          }
  
          if (!response[i]['foto']) {
            response[i]['foto'] = "https://i.imgur.com/bgoa1N1.png";
          }else{
            response[i]['foto'] = String.fromCharCode.apply(null, new Uint16Array(response[i]['foto']));
          }
        }
  
        return res.json(response);
      }
      
    } catch (error) {
      return res.json(error);
    }
  },

  async create(req, res) {
    const { nome, owner_id, uuid, foto } = req.body;

    try {
      const caderno = await knex('notebooks').insert({ nome, owner_id, uuid, foto }).returning('id');

      await knex('notebooks_users').insert({ notebooks_id: caderno[0], users_id: owner_id, permission: 'owner' });

      return res.json({ response: 'Caderno criado com sucesso!!!' });
    } catch (error) {
      return res.json({ error })
    }
  },
  async update(req, res){
    const { nome, descricao, foto, uuid } = req.body;

    try {
      const notebook = await knex('notebooks').update({ nome, descricao, foto}).where('uuid', uuid);

      if(!notebook) return res.json({response: 'Erro ao alterar caderno!!!', status: 400});

      return res.json({response: 'Caderno altarado com sucesso!!!', status: 200})
    } catch (error) {
      return res.json({response: error, status: 400})
    }
  },
  async delete(req, res){
    const uuid = req.query.uuid;

    try {
      const notebook = await knex('notebooks').update({deleted_at: knex.fn.now()}).where('uuid', uuid);

      if(!notebook) return res.json({response: 'Erro ao deletar caderno!!!', status: 400});

      const notebooks = await knex.select('nome').from('notebooks').where('uuid', uuid);
     
      await knex('trash').insert({type: 'notebooks', title: notebooks[0].nome, uuid});

      return res.json({response: 'Caderno deletado com sucesso!!!', status: 200})
    } catch (error) {
      return res.json({response: error, status: 400})
    }
  },
  async delete_share(req, res){
    const user_id = req.query.user_id;
    const notebook_id = req.query.notebook_id;

    try {
      const notebook = await knex('notebooks_users').delete().where('notebooks_id',notebook_id).where('users_id',user_id);

      if(!notebook) return res.json({response: 'Erro ao tentar deletar compatilhamento!!!', status: 400});

      return res.json({response: 'Compartilhamento deletado com sucesso!!!', status: 200})
    } catch (error) {
      return res.json({response: error, status: 400})
    }
  },
  async share(req, res) {
    const { users } = req.body;


    users.forEach(async (element) => {
      const { user_id, notebook_id, destination_user_id, name, email, permission, messege } = element;
      try {
        if (notebook_id && destination_user_id) {
          await knex('notebooks_users').delete()
            .where({ notebooks_id: notebook_id })
            .where({ users_id: destination_user_id });

          const resp = await knex('notebooks_users')
            .insert({ notebooks_id: notebook_id, users_id: destination_user_id, permission });
          if (resp) {

            const user = await knex.select('nome').from('users').where('id', user_id);
            const notebook = await knex.select('nome').from('notebooks').where('id', notebook_id);

            var transporter = nodemailer.createTransport({
              host: 'smtp.hostinger.com.br',
              port: 587,
              secure: false,
              requireTLS: true,
              auth: {
                user: 'caderno-executivo@evolutionsoft.com.br',
                pass: 'Teste123'
              }
            });
            var mailOptions = {
              from: 'caderno-executivo@evolutionsoft.com.br',
              to: email,
              subject: 'Compartilhamento - CADERNO EXECUTIVO CREA-SP',
              html: 'Olá <b>' + name + '</b>.</br><b>' + user[0].nome + '</b> compartilhou o caderno <b>' + notebook[0].nome + '</b> com você . </br>Abaixo segue o link de acesso.</br></br> <a style="background: #ffc107; color: black;border-radius: 5px; padding: 5px; text-decoration: none; font-family: sans-serif; font-size: 12px"href="http://caderno.evolutionsoft.com.br">Acessar Caderno</a></br></br> ' + messege
            };

            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                return res.json({ response: error })
              } else {
                return res.json({ response: 'Caderno compartilhado com sucesso!!!', status: 200 });
              }
            });
          }else{
            return res.json({ response: "Erro ao compartilhar caderno!!!", status: 400 });
          }
        } else {
          return res.json({ response: "Erro ao compartilhar caderno!!!", status: 400 })
        }
      } catch (error) {
        return res.json(error)
      }
    });

  },

  async share_users(req, res){
    const notebook_id = req.query.notebook_id;

    try {
      //id,nome,email,inicial
      const response = await knex.select('users.id','users.nome','users.email','notebooks_users.permission')
              .from('notebooks_users')
              .innerJoin('users','notebooks_users.users_id','users.id')
              .where('notebooks_users.notebooks_id',notebook_id);

      for(let i = 0; i < response.length ; i++){
        response[i].initial = response[i].nome.substring(0,1);
      }
      return res.json(response)
    } catch (error) {
      return res.json({error : 1})
    }
  }
}
